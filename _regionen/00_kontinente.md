---
title: Kontinente und die jeweiligen Reiche
layout: page
---
Die Welt von Samrith lässt sich in mehrere Kontinente unterteilen. Der Größte
dieser Kontinente ist Sil'Kenlis. Sil'Kenlis wird momentan noch durch der
Herrschaft der fünf alten Reiche dominiert, welche untereinander einen
wackeligen Frieden geschlossen haben. Zu diesen Reichen zählen

**Nargard**, das kalte, raue Ödland im Norden des Kontinents.

Das zentrale Großreich **Desrah**, welches zwar von der Fläche her das größte
der fünf Reiche darstellt, aber ein großes Stück seiner Fläche an die Wüste
verliert.

Im Osten liegt das Kleinste aber aufgrund von außergewöhnlichen Bodenschätzen
Wohlhabendste der fünf Reiche auf einer Halbinsel.

Nördlich von Desrah und südlich von Nargard, liegt dazwischen das Reich der
**Seestaaten**, welches durchzogen ist von kleinen und größeren Seen.

Im tiefen Süden gliedert sich das Land **Wering** an, welches aufgrund der
ausgiebigen Waldgebiete, vor allem als Heimat für Waldvölker dient, aber
wirtschaftlich und militärisch vor allem mit einer großen Seeflotte aufwarten
kann.
