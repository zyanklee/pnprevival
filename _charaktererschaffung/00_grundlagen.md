---
title: Grundlagen der Charaktererschaffung
layout: page
---

Ich habe mir noch einmal Gedanken über die Erzeugung der Charakterhintergründe
 gemacht und bin dabei auf die folgenden Fragen gestoßen, über die ihr in
 meinen Augen nachdenken solltet:

* Wie sieht euer Charakter aus.
* Wie wirkt euer Charakter auf andere?
* Wo kommt der Charakter her?
* Was will der Charakter tun?
* Warum will er das tun?
* Wen kennt dein Charakter?
* Wie sieht die Familienkonstellation deines Charakters aus?

Da einem oftmals die Ideen fehlen und euch zeitgleich so gut wie keine
Informationen über die Spielwelt zur Verfügung stehen, möchte ich euch hier
ein paar Beispiele geben, die euch vielleicht dabei helfen können. Bei Namen
eurer Charaktere, Familienmitglieder oder auch anderen NPCs aus eurem Umfeld
bitte ich euch zudem darauf zu achten, dass diese gut auszusprechen sind.
"Xschtzachetzlu" Ist bestimmt ein toller Name, führt aber in den meisten
Fällen dazu, dass einem nur ein Knoten in der Zunge bleibt.

Beispiele für die Motivation eines Charakters:

Der Charakter ist auf der Suche nach einem Erbstück seiner Großmutter,
welches bei einem Überfall, bei dem sie tragischerweise ums Leben gekommen
ist, entwendet wurde. Er trachtet danach den Tod seiner Großmutter zu rächen,
sowie das Erbe zurück zu holen und wieder in den Besitz der Familie zu bringen.

Der Charakter wurde als Kind ausgesetzt und versucht seither irgendwie in der
Welt einen Fuß fassen zu können. Wie es für Kinder von der Straße nicht
unüblich ist, hat sich der- oder diejenige früh den eher schurkischen Wegen
zugewandt. Bei einem der Raubzüge kam es zu einem unglücklichen Unfall und
eines der Opfer starb. Seither plagen den Schurken sehr mächtige Gewissensbisse
und jedes Mal, wenn er in die Nähe von Gesetzeshütern kommt wird er nervös und
sucht schnell nach einem Ausweg. Insgeheim sucht der Charakter nach einem Weg
sein Gewissen zu erleichtern, an Götter glaubt er nicht, also wendet er sich
dem Pfad der reisenden Abenteurer zu.

Bitte gebt mir die Informationen zu euren Hintergründen bis spätestens am
19.01. abends, sodass ich mir ein paar Gedanken dazu machen kann oder um
auftauchende Laserschwerter aus eurer Story zu streichen. ;) Aber keine Angst,
alles was plausibel ist werden wir auch irgendwie integrieren können.

Die Sitzungen zur Charaktererschaffung, die wir einlegen werden, eignen sich
wunderbar, um Fragen zu stellen, mit mir über eure Ideen zu diskutieren oder
mir mitzuteilen was für Informationen ihr über die Welt von mir benötigt. Ihr
dürft auch gerne selber Namen für die Städte/Dörfer/Landstriche/Flüssen
erschaffen von/aus denen eure Charaktere kommen. Wenn diese passen, dann
übernehme ich sie gerne in die Welt von Samrith.

Zum Abschluss ein Beispiel einer kurzen aber schönen Geschichte:

> Name: Tatelyn (Tate)
>
> Character Description: This young man is in his early twenties, sporting
> a short beard and grey eyes. A trace of a thin scar is evident on his left
> cheek. We is wearing green-dyed leather armor, black belt and high boots
> and has a short sword hung from his baldric.
>
> Backstory: “Tate” grew up near the village of Werth, not too far from the
> capitol. He’s fairly educated and came from a good family, although he’s
> had a taste for getting himself in over his head as a child. His Uncle
> Norhan told him stories of adventures and such, which got him “curious”
> about seeking fame and fortune.
>
> When he was fifteen, he decided to explore the Blightwood Forest near his
> home, where he was grazed on his left cheek by a small bear. Since then
> he’s slowed down “a little” in his ways as part of the town militia, if
> there’s something exciting to do and he gets wind of it, he’ll definitely
> pitch in.
>
> Now in his twenties, he’s a bit more respectful towards the wild. He
> doesn’t like being called Tatelyn, his given name – and is first to
> correct someone using it, except his parents. In his belt pouch, he
> carries a locket he found with a drawing of a rather attractive young
> girl and the word “Anna” in styled calligraphy.
>
> Perhaps he can find out who she is, who knows?

<right>— RP Made Simple</right>
