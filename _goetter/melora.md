---
title: Melora
layout: gods
group: good
metadata:
  - label: Domänen
    content: |
      Haupt: Sturm, Natur
      Untergeordnet: Wildnis, Das Meer
      Gesinnung: Neutral
  - label: Symbolik
    content: |
      Eine Muschelschale mit einem wellenartigen Wirbel darauf.
  - label: Gefilde
    content: Sil'Kenlis
  - label: Göttliche Klassifikation
    content: Gottheit
---
## Melora, Göttin der Wildnis und der See

Melora ist sowohl ein wildes Biest wie auch der friedlichste Wald,
der reißendste Wirbelstrom sowie die stille der Wüste. Waldläufer,
Jäger und Elfen verehren sie und Seefahrer bringen ihr vor ihren Reisen
Opfer dar.

### Glaubensgrundsätze

> * Bewahre die natürlichen Gebiete der Welt vor Zerstörung und Ausbeutung.
>   Stelle dich gegen die rasante Ausbreitung von Städten und Königreichen.
> * Jage abnormale Monster und andere Verunstaltungen der Natur.
> * Fürchte nicht noch verurteile die Brutalität der Natur. Lebe in Einheit
>   mit ihr.
