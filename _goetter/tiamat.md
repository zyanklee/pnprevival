---
title: Tiamat
layout: gods
group: evil
metadata:
  - label: Domänen
    content: |
      Haupt: List, Krieg
      Untergeordnet: Wohlstand, Gier, Rache, Neid
      Gesinnung: Rechtschaffen Böse
  - label: Symbolik
    content: |
      Stern bestehend auf fünf Klauen.
  - label: Gefilde
    content: Sil'Kenlis
  - label: Göttliche Klassifikation
    content: Gottheit
---
## Tiamat, Göttin des Reichtums, der Gier und der Rache

Als böse Göttin von Wohlstand, Gier und Neid ist Tiamat die Schutzherrin
der chromatischen Drachen und jener, deren Hunger nach Wohlstand jedes andere
Ziel in den Schatten stellt oder Zweifel aus dem Bewusstsein fegt.

### Glaubensgrundsätze

> * Horte Reichtum, häufe viel an und gebe wenig aus. Reichtum ist Belohnung
>   genug.
> * Verzeihe keine Kleinigkeit und lasse kein Unrecht ungestraft.
> * Nimm von anderen wonach du dich sehnst. Diejenigen, denen die Kraft fehlt
>   ihren Besitz zu verteidigen, sind es nicht wert ihn zu besitzen.
