---
title: Zehir
layout: gods
group: evil
metadata:
  - label: Domänen
    content: |
      Haupt: List, Tod
      Untergeordnet: Dunkelheit, Gift, Attentäter.
  - label: Symbolik
    content: |
      Ein Dolch in Schlangenform.
  - label: Gefilde
    content: Sil'Kenlis
  - label: Göttliche Klassifikation
    content: Gottheit
---
## Zehir, Gott der Dunkelheit und des Gifts

Zehir ist der böse Gott der Dunkelheit, des Giftes und der Attentäter.
Schlangen sind seine Lieblingsschöpfung.

### Glaubensgrundsätze

> * Versteckt euch unter dem Umhang der Nacht, damit eure Taten im
>   Verborgenen bleiben.
> * Töte in Zehirs Namen und biete jeden Mord als Opfer an.
> * Finde Wonnen und Giften und umgebe dich mit Schlangen.
