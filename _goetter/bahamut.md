---
title: Bahamut
layout: gods
group: good
metadata:
  - label: Domänen
    content: |
      Haupt: Leben, Krieg
      Untergeordnet: Gerechtigkeit, Schutz, Ehre
  - label: Symbolik
    content: |
      Ein nach links gerichteter silberner Drachenkopf auf blauem Grund.
  - label: Gefilde
    content: Sil'Kenlis
  - label: Göttliche Klassifikation
    content: Gottheit
---
## Bahamut, Der Platindrache, Erster seiner Art

Als einziges bekanntes Exemplar seiner Art und mächtigster unter den guten
Drachen wird Bahamut als Gott der Gerechtigkeit, des Schutzes und der Ehre
angebetet. Paladine von rechtschaffen guter Gesinnung verehren ihn. Bahamut
gilt unter den metallischen Drachen als der erste ihrer Art, der Schöpfer aller
anderen metallischen Drachen.

### Glaubensgrundsätze

> * Strebe nach dem höchsten Ideal von Ehre und Gerechtigkeit.
> * Sei stehts wachsam vor dem Bösen und bekämpfe es an allen Fronten.
> * Beschütze die Schwachen, befreie die Unterdrückten, und verteidige die
>   gerechte Ordnung.
