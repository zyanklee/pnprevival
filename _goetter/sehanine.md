---
title: Sehanine
layout: gods
group: good
metadata:
  - label: Domänen
    content: |
      Haupt: List
      Untergeordnet: Liebe, Mond, Herbst, Elfen, Halblinge
      Gesinnung: Chaotisch Gut
  - label: Symbolik
    content: |
      Mondsichel
  - label: Gefilde
    content: Sil'Kenlis
  - label: Göttliche Klassifikation
    content: Gottheit
---
## Sehanine, Göttin des Mondes

Pelor, Gott von Sonne und Sommer ist ebenfalls Wächter über die Zeit. Er
unterstützt jene Bedürftigen und stellt sich gegen alles was Böse ist.
Als der Herr über Ackerbau und reiche Ernten ist er die am meisten angebetete
Gottheit unter Menschen und seine Priester sind überall gerne gesehen.
Paladine und Waldläufer kann man häufig unter seinem Gefolge antreffen.

### Glaubensgrundsätze

> * Folge deinen Zielen und entdecke dein eigenes Schicksal.
> * Bleib in den Schatten, um dem brennenden Licht des Strebens nach dem Guten,
>   sowie nach dem äußeren Bösen zu entgehen.
> * Blicke immer über den Horizont und suche nach neuen Erfahrungen, lasse dich
>   niemals binden und halten, wenn du eigentlich wandern möchtest.
