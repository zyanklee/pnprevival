---
title: Lolth
layout: gods
group: evil
metadata:
  - label: Domänen
    content: |
      Haupt: List
      Untergeordnet: Schatten, Lügen, Spinnen
      Gesinnung: Chaotisch Böse
  - label: Symbolik
    content: |
      Achtzackiger Stern im Netz einer Spinne.
  - label: Gefilde
    content: Sil'Kenlis
  - label: Göttliche Klassifikation
    content: Gottheit
---
## Lolth, Göttin der Spinnen und der Lügen

Lolth ist die Göttin von Schatten, Lügen und Spinnen. Ränke und Intrigen
schmieden sind ihre Befehle und ihre Priester sind eine konstante,
zerstörerische Kraft in einer ansonsten stabilen Gesellschaft der bösen
Drow. Obwohl sie eigentlich eine Göttin ist, wird sie auch Dämonenkönigin
der Spinnen genannt.

### Glaubensgrundsätze

> * Tu was immer dir möglich ist um Macht zu erlangen und sie zu erhalten.
> * Bevorzuge Heimlichkeit und Verleumdung gegenüber der offenen Konfrontation.
> * Strebe nach der Vernichtung der Elfen bei jeder Möglichkeit.
