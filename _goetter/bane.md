---
title: Bane
layout: gods
group: evil
metadata:
  - label: Domänen
    content: |
      Haupt: Krieg
      Untergeordnet: Eroberung
      Gesinnung: Rechtschaffen Böse
  - label: Symbolik
    content: |
      Klaue mit drei nach unten gerichteten Krallen.
  - label: Gefilde
    content: Sil'Kenlis
  - label: Göttliche Klassifikation
    content: Gottheit
---
## Bane, Gott des Krieges und der Eroberung

Bane ist der böse Gott des Krieges und der Eroberung. Militärisch geprägte
Nationen von Menschen und Goblinoiden folgen ihm und erobern mit seinem
Namen auf den Lippen. Böse Kämpfer und Paladine verehren ihn.

### Glaubensgrundsätze

> * Erlaube niemals der Furcht die Macht über dich zu erlangen. Treibe sie
>   hingegen in die Herzen deiner Feinde.
> * Bestrafe Ungehorsam und Unordnung.
> * Perfektioniere deine Fähigkeiten im Kampf, ungeachtet dessen ob du ein
>  mächtiger General oder einsamer Söldner bist.
