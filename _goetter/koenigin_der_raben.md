---
title: Königin der Raben
layout: gods
group: good
metadata:
  - label: Domänen
    content: |
      Haupt: Tod, Leben
      Untergeordnet: Schicksal, Winter
      Gesinnung: Rechtschaffen Neutral
  - label: Symbolik
    content: |
      Der Kopf eines Raben im Profil
  - label: Gefilde
    content: Sil'Kenlis
  - label: Göttliche Klassifikation
    content: Gottheit
---
## Die Königin der Raben, Göttin des Todes

Ihr Name ist lange vergessen. Sie ist die Weberin des Schicksal und
Schutzherrin des Winters. Sie kennzeichnet den Tod eines jeden sterblichen
Lebens und Trauernde rufen sie während Trauerzeremonien an in der Hoffnung
sie wird über das dahingeschiedene Leben wachen und es vor dem Fluch des
Untots bewahren.

### Glaubensgrundsätze

> * Zeige kein Mitleid für jene die leiden und sterben, denn der Tod ist
>   das natürliche Ende des Lebens.
> * Bring jene zur Strecke die danach streben die Ketten des Schicksals
>   abzustreifen. Als Werkzeug der Königin der Raben, hast du Überheblichkeit
>   zu bestrafen wo auch immer du auf sie triffst.
> * Halte Ausschau nach den Kulten von Orcus und lösche sie aus wo auch immer
>   sie entstehen, denn der Dämonenprinz der Untoten strebt danach den Thron
>   der Königin der Ragen zu erklimmen.
