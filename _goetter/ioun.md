---
title: Ioun
layout: gods
group: good
metadata:
  - label: Domänen
    content: |
      Haupt: Wissen
      Untergeordnet: Fertigkeit, Prophezeiung
      Gesinnung: Neutral
  - label: Symbolik
    content: |
      Ein Haken, geformt
  - label: Gefilde
    content: Sil'Kenlis
  - label: Göttliche Klassifikation
    content: Gottheit
---
## Ioun, Göttin des Wissens

Ioun ist die Göttin des Wissens, der Fertigkeit und der Prophezeiungen. Weise,
Seher und Taktiker, aber ebenso all jene, die sich durch Wissen und mentale
Kräfte leiten lassen, verehren sie. Ioun ist die Schutzpatronin des Studierens
von Magie und all ihrer Eigenschaften und Anwendungsgebiete. Bibliotheken und
Magierakademien werden in ihrem Namen gebaut.

### Glaubensgrundsätze

> * Strebe nach der Perfektion des Geistes indem Vernunft, Wahrnehmung und
>   Emotion in Gleichklang zu einander gebracht werden.
> * Sammel, bewahre und verteile Wissen in allen Formen. Verfolge Aufklärung,
>   errichte Bibliotheken und suche nach verloren gegangener und antiker
>   Geschichte.
> * Sei allzeit wachsam vor den Anhängern Vecnas, welche nach Kontrolle des
>   Wissens streben und Geheimnisse wahren. Streite wider ihrer Pläne, decke
>   ihre Geheimnisse auf und blende sie mit dem Licht der Wahrheit und
>   Vernunft.
