---
title: Pelor
layout: gods
group: good
metadata:
  - label: Domänen
    content: |
      Haupt: Licht, Leben
      Untergeordnet: Sonne, Sommer, Agrikultur, Zeit
      Gesinnung: Neutral Gut
  - label: Symbolik
    content: |
      Ein Kreis mit sechs nach außen abstrahlenden Spitzen.
  - label: Gefilde
    content: Sil'Kenlis
  - label: Göttliche Klassifikation
    content: Gottheit
---
## Pelor, Gott der Sonne und des Ackerbaus

Pelor, Gott von Sonne und Sommer ist ebenfalls Wächter über die Zeit. Er
unterstützt jene Bedürftigen und stellt sich gegen alles was Böse ist.
Als der Herr über Ackerbau und reiche Ernten ist er die am meisten angebetete
Gottheit unter Menschen und seine Priester sind überall gerne gesehen.
Paladine und Waldläufer kann man häufig unter seinem Gefolge antreffen.

### Glaubensgrundsätze

> * Mildert Leid wo auch immer ihr es antrefft.
> * Bringe Pelors Licht an Orte der Dunkelheit, zeige Gütigkeit, Mitgefühl und Gnade.
> * Wache über das Böse.
