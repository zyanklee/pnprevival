---
title: Moradin
layout: gods
group: good
metadata:
  - label: Domänen
    content: |
      Haupt: Wissen, Krieg
      Untergeordnet: Schöpfung, Handwerk, Familie, Zwerge
      Gesinnung: Rechtschaffen Gut
  - label: Symbolik
    content: |
      Das Symbol Moradins ist ein Amboss, der meist auch mit einer Flamme
      darauf dargestellt wird.
  - label: Gefilde
    content: Sil'Kenlis
  - label: Göttliche Klassifikation
    content: Gottheit
---
## Moradin, Gott der Schöpfung

Moradin ist der Gott der Schöpfung, Handwerker und Kunsthandwerker und wird
besonders von Minenarbeitern und Schmieden angebetet. Er schnitzte die Berg
aus der vorzeitlichen Erde und ist der Wächter und Beschützer des Herdfeuers
und der Familie. Zwerge allen Standes und Ranges folgen ihm.

### Glaubensgrundsätze

> * Begegne Not und Elend mit Gleichmut und Beharrlichkeit.
> * Zeige Loyalität gegenüber deiner Familie, deines Clans, deiner Anführer
>   und deines Volkes.
> * Strebe danach deine Spur an der Welt zu hinterlassen, eine bleibendes
>   Vermächtnis. Etwas zu erzeugen das überdauert ist das höchste Gut,
>   gleichgültig ob als Schmied von Rüstungen oder als Erbauer und Herrscher
>   über eine Dynastie.
