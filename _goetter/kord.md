---
title: Kord
layout: gods
group: good
metadata:
  - label: Domänen
    content: |
      Haupt: Sturm
      Untergeordnet: Krieg, Stärke, Kampf
      Gesinnung: Chaotisch-Neutral
  - label: Symbolik
    content: |
      Ein Schwert mit einer Parierstange in Form eines Blitzes.
  - label: Gefilde
    content: Sil'Kenlis
  - label: Göttliche Klassifikation
    content: Gottheit
---
## Kord, Gott der Stärke und der Stürme

Kord ist der Sturmgott und der Herr der Schlachten. Er feiert Stärke,
Können auf dem Schlachtfeld und Donner mehr als alles andere. Kämpfer
und Athleten verehren ihn. Er ist ein launischer Gott, ungezähmt und
wild, der an Land sowie auf See Stürme ruft. Jene die auf besseres
Wetter hoffen beschwichtigen ihn mit Gebeten und feurigen Trinksprüchen.

### Glaubensgrundsätze

> * Sei stark, aber hüte dich davor sie für unnütze Zerstörung zu verwenden.
> * Sei tapfer und verachte Feigheit jeglicher Art.
> * Stell deine Macht im Kampf unter Beweis um Ruhm und Ehre zu erlangen.
