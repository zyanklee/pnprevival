---
title: Avandra
layout: gods
group: good
metadata:
  - label: Domänen
    content: |
      Haupt: List
      Untergeordnet: Veränderung/Wandel, Glück, Handel , Reisen
  - label: Symbolik
    content: |
      Drei Wellenlinien, die übereinander dargestellt werden. Oftmals Silber
      oder Braun auf grünem Grund.
  - label: Gefilde
    content: Sil'Kenlis
  - label: Göttliche Klassifikation
    content: Gottheit
---
## Avandra, Göttin des Wandels und des Glücks

Als Göttin des Wandels zieht Avandra Genugtuung aus Freiheit, Handel, Reisen
und Abenteuer. Ihre Heiligtümer sind selten in größeren Ballungsgebieten, aber
kleine Schreine ihr zu Ehren kann man an nahezu jeder Wegeskreuzung in den
Ländern in denen ihr gehuldigt wird finden. Halblinge aber auch Händler
und andere Arten an Abenteurern und Reisenden können sich dem Wunsch Avandras
Wohlwollen auf ihren teils schweren und langwierigen Reisen zu gewinnen nicht
entziehen.

### Glaubensgrundsätze

> * Glück belohnt die Kühnen - Nehmt euer Schicksal selber in die Hand und
>   Avandra schenkt euch ihr Wohlwollen.
> * Wehret euch gegen jene, die euch die Freiheit nehmen wollen und kämpft
>   für eure Unabhängigkeit.
> * Wandel ist unausweichlich, aber es ist gewissenhafte Arbeit vonnöten um
>   dafür zu Sorgen, dass Veränderung zu Gutem führt.
