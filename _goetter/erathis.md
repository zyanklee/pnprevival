---
title: Erathis
layout: gods
group: good
metadata:
  - label: Domänen
    content: |
      Haupt: Wissen
      Untergeordnet: Zivilisation, Erfindung
  - label: Symbolik
    content: "- TBD -"
  - label: Gefilde
    content: Sil'Kenlis
  - label: Göttliche Klassifikation
    content: Gottheit
---
## Erathis, Göttin der Zivilisation und des Erfindungsreichtums

Als Göttin des Erfindungsreichtums ist Erathis die Muse der großen Erfindungen,
Gründerin von Städten und Schreiberin von Gesetzen. Herrscher, Richter,
Pioniere und ergebene Bürger verehren sie und ihre Tempel zählen oft zu den
bekanntesten Plätzen der Städte von Sil'Kenlis.

### Glaubensgrundsätze

> * Arbeitet mit anderen zusammen um eure Ziele zu erreichen. Gemeinschaft und
>   Ordnung sind immer stärker als losgelöste Anstrengungen einzelner
>   Individuen.
> * Mache die Wildnis bewohnbar und verteidige das Licht der Zivilisation
>   gegenüber der herankriechenden Dunkelheit.
> * Strebe nach neuen Idee, Erfindungen, neuen Landstrichen die es zu besiedeln
>   oder Wildnis die es zu erobern gilt. Baue Maschinen, baue Städte, baue Königreiche.
