---
title: Über uns
layout: page
---

Das Leben in Samrith ist von Abenteuern für alle Lebewesen durchzogen.
Organisationen eifern um die Wette um Macht und Einfluss in ihren
Bereichen zu erlangen und Städte liegen seit Jahrhunderten miteinander
im Konflikt. Allgemein ist die Welt von Samrith frei von globalem
Rassismus, wie dem allgemein bekannten und zu viel benutzten Hass zwischen
Zwergen und Elfen. Das bedeutet nicht, dass es nicht auch Organisationen
oder Individuen gibt, die diese Ansicht teilen, aber es ist keine global
festgelegte Regel, die dies vorschreibt.

Auf dem Kontinent Sil'Kenlis sind militärische Auseinandersetzungen in den
letzten zwei Jahrzehnten nahezu komplett eingestellt worden, zu groß waren
die Verluste auf allen Seiten. Nach enormen Anstrengungen ist es einer Gruppe
Gelehrter gelungen die Oberhäupter der fünf großen Reiche dazu zu bringen einen
Rat zu etablieren, der sich um die Belange aller Reiche des Kontinents sorgen
und die richtigen Entscheidungen zum Wohle der Völker treffen soll. Diese
Arbeit ist noch nicht abgeschlossen und die kräftezehrende Aufgabe zur
Einhaltung der Balance zwischen den Interessen der Oberen ebenso wie die der
Völker zu wahren, hat gerade erst begonnen.

Ein solch großer Einschnitt in die Geschäfte eines Staates führt zu Konflikten.
Reibereien zwischen den Alten Reichen bleiben nicht aus. Um diese zu regulieren
wurde ein Heldenstreit ins Leben gerufen, der alle zwei Jahre in je einem
anderen der großen Reiche stattfinden soll.

Magie lässt sich in Samrith häufig entdecken, aber nicht jeder ist dazu in der
Lage sie zu nutzen. Jeder Bewohner weiß davon, aber gerade in Landstrichen,
die weniger dicht besiedelt sind, wird die Benutzung der Magie unter Umständen
mit Argwohn betrachtet oder die öffentliche Ausübung nicht geduldet. Es soll
sogar große Städte geben, in denen dies so gehandhabt wird und Einrichtungen
wie Teleportationskreise nicht innerhalb der Stadtmauern erlaubt sind oder
das öffentliche praktizieren arkaner Magie zu einem unangenehmen Gespräch bei
der Stadtwache führen kann.

Die Gefahren durch magische Geschöpfe, wilde Kreaturen, Dämonen, Banditen oder
anderes kennen die Völker Samriths nicht nur aus den Schauergeschichten, die
sie ihren Kindern oder sich selbst des nachts am Lagerfeuer erzählen, sondern
auch von ihren eigenen Reisen. Nicht von ungefähr werden daher lieber größere
Gruppen gebildet um nach dem Grundsatz "mehr Sicherheit in großen Gruppen" oder
"dadurch sinkt die Wahrscheinlichkeit, dass es mich erwischt, sollte es hart
auf hart kommen", eine Reise wohlbehalten überstehen zu können. Immer wieder
hört man von Drachen in den Gebirgen oder tiefen Sümpfen, ebenso wie von
Überfällen durch Goblins, Banditen oder Orks. Die großen Straßen der Reiche
sind allerdings so gut abgesichert, dass sich Reisen dort auch in kleineren
Gruppen lohnt. Gefährlich wird es erst, wenn man von diesen abweichen muss,
aber wer würde schon auf die Idee kommen die warme Geborgenheiten eines Kamins
und ein gemütliches Nachtlager gegen die Ungemütlichkeit der rauen Wildnis
einzutauschen?
